const sayHello = name => console.log("Hi " + name);

const sayHello2 = (name, phrase) => console.log("Hi " + name + phrase);

const sayHello3 = () => console.log("Hi ");

const sayHello4 = name => "Hi " + name;

const sayHello5 = (name, phrase = ' como estas') => console.log("Hi " + name + phrase);

sayHello('Adri');

sayHello2('Adri', 'como estas');

sayHello3();

console.log(sayHello4('Adri')); 

sayHello5('Carmen');
sayHello5('Carmen', ' Como estas')

function checkInput(cb, ...args) {
  let hasEmpty = false;
  for(const text of args) {
    if (!text) {
      hasEmpty = true;
      break
    }
  }
  if(!hasEmpty) {
    cb();
  } 
}

checkInput(() => {
  console.log('All not empty');
},'Hi', 'Adri', 'Como', 'Estas', 'Not empty')